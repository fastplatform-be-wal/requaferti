import os
import sys
from typing import Dict, Union
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    APP_DIR: Path = PurePath(__file__).parent

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"
    EPSG_SRID_BELGE_72: str = "4313"
    EPSG_SRID_BELGIAN_LAMBERT_72: str = "31370"

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "addon-requaferti-fertilization-requaferti"

    REQUAFERTI_COMPUTE_N_CACHE_MAX_SIZE: int = 10000
    REQUAFERTI_COMPUTE_N_CACHE_TTL: int = 300  # seconds
    REQUAFERTI_COMPUTE_N_URL: str = "https://www.requaferti.requasud.be/requaferti_ws"
    REQUAFERTI_CONFIG_CACHE_TTL: int = 3600
    REQUAFERTI_CONFIG_DICO_CONCURRENCY: int = 3
    REQUAFERTI_CONFIG_DICO_URL: str = "https://www.requaferti.requasud.be/requaferti_dico_ws/dico"
    REQUAFERTI_CONFIG_LISTE_DICOS: str = "age_prairie,age_luzerne,saison_destruction,rang_prairie,recouvrement_colza,rang_luzerne,partie_aerienne,culture_colza,arriere_effet_luzerne,arriere_effet,type_recolte,freq_apport,type_eff,type_cult,type_production,temp_air,culture_effet_cipan,cat_animal,effet_cipan,groupe_cat_animal,effet_prec,culture_effet_prec,culture_epinard,culture_stade_vegetation,hauteur_epinard,stade_vegetation,besoin_n,besoin_n_unite,teneur_n,type_cult_rqs_spw"
    REQUAFERTI_DEFAULT_TIMEOUT: int = 10
    REQUAFERTI_PASSWORD: str
    REQUAFERTI_USERNAME: str

    PDF_GENERATOR_ENDPOINT = str(os.getenv("PDF_GENERATOR_ENDPOINT", "http://0.0.0.0:7010/generate_pdf"))

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()
