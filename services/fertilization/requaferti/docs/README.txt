*******************************************
** 	REQUAFERTI Webservice		 **
*******************************************

WebService
------------

Architecture: REST API

URL : https://www.requaferti.requasud.be/requaferti_ws

INPUT Format : array ["key"=>value]
OUTPUT Format : JSON


Fichier dictionnaire
--------------------------

*** Les fichiers commençant par des chiffres, sont les données à utiliser dans le tableau d'input.
	Lors d'échange d'information, il faut utiliser les colonnes "id"
	
*** Les fichiers commençant par une lettre ( A_,B_,C_,...) 	representent les différentes combinaisons acceptées en association pour obtenir les données de calcul.

Le détail des champs à transmettre au webservice se trouve dans le fichier : list_field_ws.txt
le détail des champs avec leurs libelles en français, unité, champs requis sont disponible dans  : list_field.xlsx



