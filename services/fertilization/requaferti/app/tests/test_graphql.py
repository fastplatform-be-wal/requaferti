from pathlib import Path
import glob
import json
from unittest import TestCase

from fastapi.testclient import TestClient

from app.main import app


def test_graphql():

    with TestClient(app) as client:
        filepaths = glob.glob(str(Path(__file__).parent / "graphql/*.graphql"))
        
        for filepath in filepaths:
            filepath = Path(filepath)

            print(">", filepath.name)

            # Read the query and its corresponding expected json
            query = filepath.read_text()
            if filepath.with_suffix(".variables.json").exists():
                variables = json.loads(filepath.with_suffix(".variables.json").read_text())
            else:
                variables = {}

            # Submit
            response = client.post(
                "/graphql", json={"query": query, "variables": variables}
            )

            # Assert results are identical (if provided) or just there with no errors
            if filepath.with_suffix(".json").exists():
                expected_json = json.loads(filepath.with_suffix(".json").read_text())
                TestCase().assertDictEqual(response.json(), expected_json)
            else:
                TestCase().assertTrue("data" in response.json())
                TestCase().assertFalse("errors" in response.json())
