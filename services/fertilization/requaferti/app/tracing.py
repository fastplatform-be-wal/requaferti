from fastapi import FastAPI

from opentelemetry import trace
from opentelemetry.exporter.zipkin.json import Protocol
from opentelemetry.exporter.zipkin.json import ZipkinExporter
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry.propagate import set_global_textmap
from opentelemetry.propagate import inject
from opentelemetry.propagators.b3 import B3SingleFormat
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.sampling import ParentBased, TraceIdRatioBased

from .settings import config


class Tracing():
    @staticmethod
    def init(app: FastAPI):

        zipkin_exporter = ZipkinExporter(
            version= Protocol.V2,
            endpoint=config.OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT,
        )

        sampler = TraceIdRatioBased(config.OPENTELEMETRY_SAMPLING_RATIO)

        trace.set_tracer_provider(
            TracerProvider(
                    resource=Resource.create(
                        {
                            SERVICE_NAME: config.OPENTELEMETRY_SERVICE_NAME
                        }
                    ),
                    sampler=ParentBased(sampler)
                )
        )

        trace.get_tracer_provider().add_span_processor(
            BatchSpanProcessor(zipkin_exporter)
        )

        set_global_textmap(B3SingleFormat())

        FastAPIInstrumentor.instrument_app(app)

    @staticmethod
    def inject_context(headers: dict):
        inject(Tracing._setter, headers)

    @staticmethod
    def _setter(carrier: dict, key: str, value: str):
        carrier[key] = value
